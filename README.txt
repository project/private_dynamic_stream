Private Dynamic Stream Wrapper
Learn more at http://drupal.org/project/private_dynamic_stream.

Settings
  - Set your private dynamic file system path at admin/config/media/file-system.
  - Select "Private Dynamic files" for the upload destination when creating
    any file or image field.
